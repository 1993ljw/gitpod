# 2. Use GitLab for Source Repository

Date: 2023-02-13

## Status

Accepted

## Context

Github is not available for on-premise
Gitlab can be used in on-premise hosting.
Gitlab is open source

## Decision

We decide Source Repo to Gitlab Cloud

## Consequences

We will use git cloud until MVP version.
Infra team will setup on-premise gitlab server